#!/usr/bin/env bash

export $(egrep -v '^#' .env | xargs)

psql -d $DB_NAME -c "DROP SCHEMA public CASCADE; CREATE SCHEMA public;"

psql -d $DB_NAME -f ./dumps/pcrm.sql
