#!/usr/bin/env bash

export $(egrep -v '^#' .env | xargs)

mkdir -p dumps

pg_dump $DB_NAME -x -O > ./dumps/push.sql

scp ./dumps/push.sql $PRD_USER@$PRD_HOST:~/dumps/push.sql

ssh $PRD_HOST -l $PRD_USER -C "pg_dump $PRD_DB_NAME -x -O > ~/dumps/push_backup.sql & psql -d $PRD_DB_NAME -c \"DROP SCHEMA public CASCADE; CREATE SCHEMA public;\" & psql -d $PRD_DB_NAME -f ~/dumps/push.sql"
