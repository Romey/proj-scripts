#!/usr/bin/env bash

export $(egrep -v '^#' .env | xargs)

mkdir -p dumps

ssh $PRD_HOST -l $PRD_USER -C "pg_dump $PRD_DB_NAME -x -O > ~/dumps/dump.sql"

scp $PRD_USER@$PRD_HOST:~/dumps/dump.sql ./dumps/

psql -d $DB_NAME -c "DROP SCHEMA public CASCADE; CREATE SCHEMA public;"

psql -d $DB_NAME -f ./dumps/dump.sql
