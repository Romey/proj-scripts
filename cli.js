#!/usr/bin/env node

const { spawn } = require('child_process');
const path = require('path');
const program = require('commander');


addScriptCmd('db:pull', 'pull_db.sh');
addScriptCmd('db:push', 'push_db.sh');
addScriptCmd('db:restore', 'restore_db.sh');
addCmd('db:new', 'yarn', ['knex', 'migrate:make']);
addCmd('db:migrate', 'yarn', ['knex', 'migrate:latest']);

program.parse(process.argv);


function addScriptCmd(cmd, fileName) {
  const fullPath = path.resolve(__dirname, 'sh', fileName);
  
  program
    .command(cmd)
    .action(() => {
      spawn('bash', [fullPath], { stdio: 'inherit' });
    });
}

function addCmd(cmd, command, args) {
  program
    .command(cmd)
    .action(() => {
      spawn(command, args, { stdio: 'inherit' });
    });
}
